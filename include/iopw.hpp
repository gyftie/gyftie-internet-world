#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/crypto.hpp>
#include <eosio/multi_index.hpp>
#include <eosio/singleton.hpp>

#include <cryptoutil.hpp>

#include <math.h>
#include <string>

using namespace eosio;
using cryptoutil::hash;
using std::string;
using std::map;

class[[eosio::contract("iopw")]] iopw : public contract
{
public:
   using contract::contract;

   [[eosio::action]] void reset ();
   [[eosio::action]] void backupassets ();
   [[eosio::action]] void restoreassts ();
   [[eosio::action]] void delconfig ();
   [[eosio::action]] void rembalances (const name& account);
   
   [[eosio::action]] void updateissuer (const symbol& asset_symbol, 
                                          const name& new_issuer);

   [[eosio::action]] void delstat (const symbol& token_symbol);

   [[eosio::action]] void setconfig(const name& bidding_token_contract, 
                                    const symbol& bidding_token_symbol,
                                    const float& default_cost_multiplier,
                                    const asset& default_base_cost,
                                    const float& default_recovery_share);

                                    // const name& initial_owner,
                                    // const asset& initial_price);
     
   [[eosio::action]] void create(const name &creator,
                                 const string& asset_name,
                                 const string& asset_description,
                                 const string& asset_category,
                                 const string& asset_subcategory,
                                 const symbol& asset_symbol,
                                 const map<string, string> attribute_pairs);

   [[eosio::action]] void addattribute (const uint64_t& asset_id, 
                                       const string& attribute_name,
                                       const string& attribute_value);

   [[eosio::action]] void remattribute (const uint64_t& asset_id, 
                                       const string& attribute_name);

   [[eosio::action]] void editattr (const uint64_t& asset_id, 
                                    const string& attribute_name,
                                    const string& attribute_value);

   [[eosio::action]] void setattrs (const uint64_t& asset_id,
                                    const map<string, string> attributes);

   // [[eosio::action]] void setprice (const uint64_t& asset_id,
   //                                  const asset&   price);

   // [[eosio::action]] void setbid (const uint64_t& asset_id,
   //                                  const name& bidder,
   //                                  const asset&   price);

   [[eosio::action]] void buy (const name& buyer, 
                              const asset& tlosgft_to_spend,
                              const symbol& symbol_to_buy) ;

   [[eosio::action]] void sell (const name& seller,
                                 const asset& amount_to_sell);

   [[eosio::action]] void eraseasset (const uint64_t& asset_id);

   [[eosio::action]] void issue(const name &to, const asset &quantity, const string &memo);
   [[eosio::action]] void retire(const asset &quantity, const string &memo);
   [[eosio::action]] void transfer(const name &from,
                                   const name &to,
                                   const asset &quantity,
                                   const string &memo);
   [[eosio::action]] void open(const name &owner, const symbol &symbol, const name &ram_payer);
   [[eosio::action]] void close(const name &owner, const symbol &symbol);
   static asset get_supply(const name &token_contract_account, const symbol_code &sym_code)
   {
      stats statstable(token_contract_account, sym_code.raw());
      const auto &st = statstable.get(sym_code.raw());
      return st.supply;
   }

   static asset get_balance(const name &token_contract_account, const name &owner, const symbol_code &sym_code)
   {
      accounts accountstable(token_contract_account, owner.value);
      const auto &ac = accountstable.get(sym_code.raw());
      return ac.balance;
   }

   using create_action = eosio::action_wrapper<"create"_n, &iopw::create>;
   using issue_action = eosio::action_wrapper<"issue"_n, &iopw::issue>;
   using retire_action = eosio::action_wrapper<"retire"_n, &iopw::retire>;
   using transfer_action = eosio::action_wrapper<"transfer"_n, &iopw::transfer>;
   using open_action = eosio::action_wrapper<"open"_n, &iopw::open>;
   using close_action = eosio::action_wrapper<"close"_n, &iopw::close>;

   [[eosio::on_notify("*::transfer")]]
   void deposit ( const name& from, const name& to, const asset& quantity, const string& memo );

private:

   const string   COST_MULTIPLIER      = "COST_MULTIPLIER";
   const string   BASE_COST            = "BASE_COST";
   const string   RECOVERY_SHARE       = "RECOVERY_SHARE";

   struct [[eosio::table]] account
   {
      asset balance;

      uint64_t primary_key() const { return balance.symbol.code().raw(); }
   };

   struct [[eosio::table]] currency_stats
   {
      asset                      supply;
      asset                      max_supply;
      name                       issuer;

      uint64_t primary_key() const { return supply.symbol.code().raw(); }
   };

   typedef eosio::multi_index<"accounts"_n, account> accounts;
   typedef eosio::multi_index<"stat"_n, currency_stats> stats;

   struct [[ eosio::table, eosio::contract("iopw") ]] config
   {
      // a general purpose settings map
      std::map<name, uint8_t>     settings               ;

      name                    bidding_token_contract     = "gyftietokens"_n;
      symbol                  bidding_token_symbol       = symbol ("TLOSGFT", 8);

      // updated with data structure
      float                   cost_multiplier            = 1.0100000000; // c
      asset                   base_cost                  = asset { 1000000, symbol("TLOSGFT", 8)}; // b
      float                   recovery_share             = 0.990000000; // s                   
   };

   typedef singleton<"configs"_n, config> config_table;
   // this config placeholder makes it easier to query parameters (bug in EOSIO?)
   typedef multi_index<"configs"_n, config> config_placeholder;
   
   struct [[ eosio::table, eosio::contract("iopw") ]] AssetOld 
   {
      uint64_t                   asset_id             ;
      string                     asset_name           ;
      symbol                     asset_symbol         ;
      string                     asset_description    ;
      string                     asset_category       ;
      string                     asset_subcategory    ;
      name                       creator              ;
      name                       owner                ;
      asset                      offer_price          = asset { 0, symbol ("TLOSGFT", 8)};
      name                       highest_bidder       ;
      asset                      highest_bid          = asset { 0, symbol ("TLOSGFT", 8)};

      // needs to be updated with data structure
      // float                      cost_multiplier      = 1.0100000000; // c
      // asset                      base_cost            = asset { 1000000, symbol("TLOSGFT", 8)}; // b
      // float                      recovery_share       = 0.990000000; // s

      time_point_sec  created_date    = time_point_sec(current_time_point());
      time_point_sec  updated_date    = time_point_sec(current_time_point());

      std::map<string, string>   owner_attributes        ;
      std::map<string, string>   immutable_attributes    ;

      uint64_t    primary_key() const { return asset_id; }
      uint64_t    by_created () const { return created_date.sec_since_epoch(); }
      uint64_t    by_updated () const { return updated_date.sec_since_epoch(); }
      uint64_t    by_creator () const { return creator.value; }
      uint64_t    by_symbol () const { return asset_symbol.code().raw(); }
      uint64_t    by_category () const { return hash(asset_category); }
      uint64_t    by_subcategory () const { return hash(asset_subcategory); }
   };

   typedef multi_index<"assetsold"_n, AssetOld,
      indexed_by<"bycreated"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_created>>,
      indexed_by<"byupdated"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_updated>>,
      indexed_by<"bycreator"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_creator>>,
      indexed_by<"bysymbol"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_symbol>>,
      indexed_by<"bycategory"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_category>>,
      indexed_by<"bysubcat"_n, const_mem_fun<AssetOld, uint64_t, &AssetOld::by_subcategory>>
   > asset_table_old;


   struct [[ eosio::table, eosio::contract("iopw") ]] Asset 
   {
      uint64_t                   asset_id             ;
      string                     asset_name           ;
      symbol                     asset_symbol         ;
      string                     asset_description    ;
      string                     asset_category       ;
      string                     asset_subcategory    ;
      name                       creator              ;
      // name                       owner                ;
      // asset                      offer_price          = asset { 0, symbol ("TLOSGFT", 8)};
      // name                       highest_bidder       ;
      // asset                      highest_bid          = asset { 0, symbol ("TLOSGFT", 8)};

      // needs to be updated with data structure
      float                      cost_multiplier      = 1.0100000000; // c
      asset                      base_cost            = asset { 1000000, symbol("TLOSGFT", 8)}; // b
      float                      recovery_share       = 0.990000000; // s

      time_point_sec  created_date    = time_point_sec(current_time_point());
      time_point_sec  updated_date    = time_point_sec(current_time_point());

      std::map<string, string>   owner_attributes        ;
      std::map<string, string>   immutable_attributes    ;

      uint64_t    primary_key() const { return asset_id; }
      uint64_t    by_created () const { return created_date.sec_since_epoch(); }
      uint64_t    by_updated () const { return updated_date.sec_since_epoch(); }
      uint64_t    by_creator () const { return creator.value; }
      uint64_t    by_symbol () const { return asset_symbol.code().raw(); }
      uint64_t    by_category () const { return hash(asset_category); }
      uint64_t    by_subcategory () const { return hash(asset_subcategory); }
   };

   typedef multi_index<"assets"_n, Asset,
      indexed_by<"bycreated"_n, const_mem_fun<Asset, uint64_t, &Asset::by_created>>,
      indexed_by<"byupdated"_n, const_mem_fun<Asset, uint64_t, &Asset::by_updated>>,
      indexed_by<"bycreator"_n, const_mem_fun<Asset, uint64_t, &Asset::by_creator>>,
      indexed_by<"bysymbol"_n, const_mem_fun<Asset, uint64_t, &Asset::by_symbol>>,
      indexed_by<"bycategory"_n, const_mem_fun<Asset, uint64_t, &Asset::by_category>>,
      indexed_by<"bysubcat"_n, const_mem_fun<Asset, uint64_t, &Asset::by_subcategory>>
   > asset_table;

   void sub_balance(const name &owner, const asset &value);
   void add_balance(const name &owner, const asset &value, const name &ram_payer);

   struct [[ eosio::table, eosio::contract("iopw") ]] balance
   {
      asset funds;
      name token_contract;
      uint64_t primary_key() const { return funds.symbol.code().raw(); }
   };

   typedef multi_index<"balances"_n, balance> balance_table;

   config get_config () {
      config_table      config_s (get_self(), get_self().value);
      return config_s.get_or_create (get_self(), config());
   }

   asset adjust_asset (const asset& original_asset, const float& adjustment) {
      return asset { static_cast<int64_t> (original_asset.amount * adjustment), original_asset.symbol };
   }

   float asset_to_float (const asset& original_asset) {
      return (float) original_asset.amount / (float) pow(10, original_asset.symbol.precision());
   }

   asset get_purchase_qty (const asset& tlosgft_amount, const symbol& purchase_symbol) {
      // for TLOSGFT amount = p,
      // purchase qty = ln( ( c^n + (  p * ln(c) / b  ) ) / (c^n)) / ln(c)
      //                      part1        part2             part1       part2
      
      asset supply = get_supply (purchase_symbol);
      Asset asset_record = get_Asset (purchase_symbol);

      print (" Base Cost               : ", asset_record.base_cost.to_string(), "\n");
      print (" Cost Multiplier         : ", std::to_string(asset_record.cost_multiplier), "\n");

      float p = asset_to_float (tlosgft_amount);
      float n = asset_to_float (supply);
      float c = asset_record.cost_multiplier;
      float b = asset_to_float(asset_record.base_cost);

      print (" TLOSGFT Amount          : ", std::to_string(p), "\n");
      print (" Asset Supply            : ", std::to_string(n), "\n");
      
      float purchase_qty = log ( ( pow(c,n) + (p* log(c) / b ) ) / pow(c,n)) / log(c);
      print (" Purchase Quantity       : ", std::to_string(purchase_qty), "\n");

      return asset { static_cast<int64_t>(purchase_qty * pow (10, purchase_symbol.precision())), purchase_symbol };
      // return asset { 0, purchase_symbol };
   }

   asset get_sales_price (const asset& amount_to_sell) {

      // for amount_to_sell = q,
      // price = s * b / ( ln(c)) * (c^n-c^(n-q))
      
      asset supply = get_supply (amount_to_sell.symbol);
      Asset asset_record = get_Asset (amount_to_sell.symbol);

      print (" Base Cost               : ", asset_record.base_cost.to_string(), "\n");

      float q = asset_to_float (amount_to_sell);
      float n = asset_to_float (supply);
      float c = asset_record.cost_multiplier;
      float b = asset_to_float(asset_record.base_cost);
      float s = asset_record.recovery_share;

      print (" Amount to Sell          : ", std::to_string(q), "\n");
      print (" Asset Supply            : ", std::to_string(n), "\n");
      print (" Cost Multiplier         : ", std::to_string(c), "\n");
      print (" Recovery Share          : ", std::to_string(s), "\n");
      
      float sales_price = s * b / ( log(c)) * ( pow(c,n) - pow(c,n-q));
      print (" Sales Price             : ", std::to_string(sales_price), "\n");

      return asset { static_cast<int64_t>(sales_price * pow (10, asset_record.base_cost.symbol.precision())), asset_record.base_cost.symbol };

      // return amount_to_sell;

   }

   // asset get_sales_price (const asset& amount_to_sell) {

   //    asset supply = get_supply (amount_to_sell.symbol);
   //    Asset asset_record = get_Asset (amount_to_sell.symbol);

   //    print (" Base Cost               : ", asset_record.base_cost.to_string(), "\n");
   //    print (" Cost Multiplier         : ", std::to_string(asset_record.cost_multiplier), "\n");

   //    auto supply_float   = asset_to_float(supply);
   //    print (" Existing Token Supply   : ", supply.to_string(), "\n");
   //    // print (" Token Supply as Float   : ", std::to_string(supply_float), "\n");

   //    auto purchase_float = asset_to_float(amount_to_sell);
   //    print (" Amount to Sell           : ", amount_to_sell.to_string(), "\n");
   //    // print (" Amount to Buy           : ", std::to_string(purchase_float), "\n");

   //    auto part3a    = pow (asset_record.cost_multiplier, supply_float);
   //    // print (" Part 3a as Float        : ", std::to_string(part3a), "\n");

   //    auto part3b    = pow (asset_record.cost_multiplier, (float) supply_float + (float) purchase_float);
   //    // print (" Part 3b as Float        : ", std::to_string(part3b), "\n");

   //    double part3 = part3b - part3a;
   //    // print (" Part 3 as Float         : ", std::to_string(part3), "\n");

   //    asset full_price = asset { static_cast<int64_t>(asset_record.base_cost.amount / log(asset_record.cost_multiplier) * part3),
   //                               asset_record.base_cost.symbol };

   //    return full_price;
   // }

   Asset get_Asset (const symbol& asset_symbol) {
      asset_table a_t (get_self(), get_self().value);
      auto symbol_index_a_t = a_t.get_index<"bysymbol"_n>();
      auto a_itr = symbol_index_a_t.find (asset_symbol.code().raw());
      check (a_itr != symbol_index_a_t.end(), "Asset token symbol not found: " + asset_symbol.code().to_string());
      return *a_itr;
   }

   asset get_supply (const symbol& supply_symbol) {
      stats statstable( get_self(), supply_symbol.code().raw() );
      auto s_itr = statstable.find( supply_symbol.code().raw() );
      check (s_itr != statstable.end(), "Token symbol not found: " + supply_symbol.code().to_string());

      return s_itr->supply;
   }
   
   void confirm_balance (const name& account, 
                           const asset& amount) {

      // config_table      config_s (get_self(), get_self().value);
      // config c = config_s.get_or_create (get_self(), config());
      // check (quantity.symbol == c.bidding_token_symbol, "Only deposits of then bidding token symbol are allowed. Bidding token: " +
      //    c.bidding_token_symbol.code().to_string() + "; You sent: " + quantity.symbol.code().to_string());

      balance_table balances(get_self(), account.value);
      auto b_itr = balances.find(amount.symbol.code().raw());
      check (b_itr != balances.end(), "No balance exists in contract for account: " + account.to_string() + "; symbol: " +
         amount.symbol.code().to_string());
      check (b_itr->funds >= amount, "Insufficient funds in contract. Account: " + account.to_string() + " has balance of " +
         b_itr->funds.to_string() + ".  Transaction requires: " + amount.to_string());
   }

   void sendfrombal (const name& from,
                     const name& to,
                     const asset& token_amount,
                     const string& memo) 
   {
      if (token_amount.amount > 0) {   
         balance_table bal_table (get_self(), from.value);
         auto it = bal_table.find(token_amount.symbol.code().raw());
         eosio::check (it != bal_table.end(), "Sender does not have a balance within the contract." );
         eosio::check (it->funds >= token_amount, "Insufficient balance.");

         bool remove_record = false;
         bal_table.modify (it, get_self(), [&](auto &b) {
            if (b.funds == token_amount) {
               remove_record = true;
            }
            b.funds -= token_amount;
         });

         require_recipient (from);

         if (to != get_self()) {
            action(
               permission_level{get_self(), "active"_n},
               it->token_contract, "transfer"_n,
               std::make_tuple(get_self(), to, token_amount, memo))
            .send();   
         }
            
         if (remove_record) {
            bal_table.erase (it);
         }
      }  
   }

};
/** @}*/ // end of @defgroup eosiotoken eosio.token
