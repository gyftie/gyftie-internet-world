
# update / redeploy
cleos -u https://test.telos.kitchen set contract gyftieassets iopw/

cleos -u https://test.telos.kitchen push action gyftieassets backupassets '[]' -p gyftieassets

cleos -u https://test.telos.kitchen push action gyftieassets delconfig '[]' -p gyftieassets

cleos -u https://test.telos.kitchen push action gyftieassets reset '[]' -p gyftieassets

# compile / update / redeploy

cleos -u https://test.telos.kitchen push action gyftieassets setconfig '["gyftietokens", "8,TLOSGFT", "1.0100000000", "0.01000000 TLOSGFT", "0.99000000000"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets restoreassts '[]' -p gyftieassets


cleos -u https://test.telos.kitchen push action gyftietokens issue '["gyftietokens", "0.05000000 TLOSGFT", "memo"]' -p gyftietokens
cleos -u https://test.telos.kitchen push action gyftietokens transfer '["gyftietokens", "agyftieuser1", "0.05000000 TLOSGFT", "memo"]' -p gyftietokens

cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,GOMEZ", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,KIMK", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,MESSI", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,KYLIEJ", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,BEYONCE", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,AGRANDE", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,THEROCK", "gyftieassets"]' -p gyftieassets

cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,GOMEZ", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,GOMEZ", "gyftieassets"]' -p gyftieassets
cleos -u https://test.telos.kitchen push action gyftieassets updateissuer '["8,GOMEZ", "gyftieassets"]' -p gyftieassets


cleos -u https://test.telos.kitchen push action gyftietokens transfer '["agyftieuser1", "gyftieassets", "0.05000000 TLOSGFT", "memo"]' -p agyftieuser1


cleos -u https://test.telos.kitchen push action gyftieassets buy '["agyftieuser1", "0.01000000 TLOSGFT", "8,GOMEZ"]' -p agyftieuser1
cleos -u https://test.telos.kitchen push action gyftieassets sell '["agyftieuser1", "0.00010000 GOMEZ"]' -p agyftieuser1


cleos -u https://test.telos.kitchen get table gyftieassets agyftieuser1 accounts