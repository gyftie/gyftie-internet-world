#include <iopw.hpp>

void iopw::reset () 
{
   require_auth (get_self());
   config_table      config_s (get_self(), get_self().value);
   config_s.remove();

   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.begin ();
   while (a_itr != a_t.end()) {
      
      stats statstable( get_self(), a_itr->asset_symbol.code().raw() );
      auto existing = statstable.find( a_itr->asset_symbol.code().raw() );
      if (existing != statstable.end()) {
         statstable.erase (existing);
      }
      a_itr = a_t.erase (a_itr);
   }

   accounts accts_t (get_self(), get_self().value);
   auto accts_itr = accts_t.begin();
   while (accts_itr != accts_t.end()) {
      accts_itr = accts_t.erase (accts_itr);
   }
}

void iopw::rembalances (const name& account) 
{
   require_auth (get_self());

   accounts accts_t (get_self(), account.value);
   auto accts_itr = accts_t.begin();
   while (accts_itr != accts_t.end()) {
      accts_itr = accts_t.erase (accts_itr);
   }
}

void iopw::updateissuer  (const symbol& asset_symbol, 
                           const name& new_issuer)
{
   require_auth (get_self());
   stats statstable( get_self(), asset_symbol.code().raw() );
   auto existing = statstable.find( asset_symbol.code().raw() );
   check (existing != statstable.end(), "Asset symbol does not exist.");
   statstable.modify (existing, get_self(), [&](auto &s) {
      s.issuer = new_issuer;
   });
}

void iopw::delconfig () 
{
   require_auth (get_self());
   config_table      config_s (get_self(), get_self().value);
   config_s.remove();
}

void iopw::backupassets () 
{   
   require_auth (get_self());

   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.begin ();
   check (a_itr != a_t.end(), "There are no assets to backup.");

   asset_table_old a_t_old (get_self(), get_self().value);
      
   while (a_itr != a_t.end()) {
      a_t_old.emplace (get_self(), [&](auto &a) {
         a.asset_id              = a_itr->asset_id;
         a.asset_name            = a_itr->asset_name;
         a.asset_symbol          = a_itr->asset_symbol;
         a.asset_description     = a_itr->asset_description;
         a.asset_category        = a_itr->asset_category;
         a.asset_subcategory     = a_itr->asset_subcategory;
         a.creator               = a_itr->creator;
         a.created_date          = a_itr->created_date;
         a.updated_date          = a_itr->updated_date;
         a.owner_attributes      = a_itr->owner_attributes;
         a.immutable_attributes  = a_itr->immutable_attributes;
      });
      a_itr = a_t.erase (a_itr);
   }
}

void iopw::restoreassts () 
{
   require_auth (get_self());

   asset_table a_t (get_self(), get_self().value);

   asset_table_old a_t_old (get_self(), get_self().value);
   auto a_itr_old = a_t_old.begin ();
   check (a_itr_old != a_t_old.end(), "There are no assets to backup.");
      
   while (a_itr_old != a_t_old.end()) {
      a_t.emplace (get_self(), [&](auto &a) {
         a.asset_id              = a_itr_old->asset_id;
         a.asset_name            = a_itr_old->asset_name;
         a.asset_symbol          = a_itr_old->asset_symbol;
         a.asset_description     = a_itr_old->asset_description;
         a.asset_category        = a_itr_old->asset_category;
         a.asset_subcategory     = a_itr_old->asset_subcategory;
         a.creator               = a_itr_old->creator;
         a.created_date          = a_itr_old->created_date;
         a.updated_date          = a_itr_old->updated_date;
         a.owner_attributes      = a_itr_old->owner_attributes;
         a.immutable_attributes  = a_itr_old->immutable_attributes;
      });
      a_itr_old = a_t_old.erase (a_itr_old);
   }
}

void iopw::eraseasset (const uint64_t& asset_id)
{
   require_auth (get_self());

   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.find (asset_id);
   check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

   a_t.erase (a_itr);
}

void iopw::delstat (const symbol& token_symbol) 
{
   stats statstable( get_self(), token_symbol.code().raw() );
   auto s_itr = statstable.find( token_symbol.code().raw() );
   check (s_itr != statstable.end(), "Token symbol not found.");

   require_auth (s_itr->issuer);
   statstable.erase (s_itr);
}

void iopw::setconfig (const name& bidding_token_contract, 
                        const symbol& bidding_token_symbol,
                        const float& default_cost_multiplier,
                        const asset& default_base_cost,
                        const float& default_recovery_share)

                        // const name& initial_owner,
                        // const asset& initial_price)
{
   require_auth (get_self());

   check (is_account(bidding_token_contract), "Bidding token contract " + 
      bidding_token_contract.to_string() + " is not a valid account.");

   check (bidding_token_symbol.is_valid(), "Bidding token symbol " + 
      bidding_token_symbol.code().to_string() + " is not a valid symbol.");

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   c.bidding_token_contract   = bidding_token_contract;
   c.bidding_token_symbol     = bidding_token_symbol;

   c.cost_multiplier          = default_cost_multiplier;
   c.base_cost                = default_base_cost;
   c.recovery_share           = default_recovery_share;

   // c.initial_owner         = initial_owner;
   // c.initial_price         = initial_price;
   config_s.set(c, get_self());
}

// need to identify permission to this
void iopw::addattribute (const uint64_t& asset_id, 
                        const string& attribute_name,
                        const string& attribute_value) 
{
   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.find (asset_id);
   check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

   auto attr_itr = a_itr->owner_attributes.find(attribute_name);
   check (attr_itr == a_itr->owner_attributes.end(), "Attribute already exists.  Name: " + attribute_name + 
      "; Value: " + a_itr->owner_attributes.at(attribute_name) + ". Use editattr if you want to Edit it.");

   a_t.modify (a_itr, get_self(), [&](auto &a) {
      a.owner_attributes[attribute_name] = attribute_value; 
   });
}

// need to identify permission to this

void iopw::remattribute (const uint64_t& asset_id, 
                           const string& attribute_name) 
{
   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.find (asset_id);
   check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

   auto attr_itr = a_itr->owner_attributes.find(attribute_name);
   check (attr_itr != a_itr->owner_attributes.end(), "Attribute does not exist.  Name: " + attribute_name);

   a_t.modify (a_itr, get_self(), [&](auto &a) {
      a.owner_attributes.erase (a.owner_attributes.find(attribute_name)); 
   });
}

void iopw::editattr (const uint64_t& asset_id, 
                     const string& attribute_name,
                     const string& attribute_value) 
{
   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.find (asset_id);
   check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

   auto attr_itr = a_itr->owner_attributes.find(attribute_name);
   check (attr_itr != a_itr->owner_attributes.end(), "Attribute does not exist.  Name: " + attribute_name);

   a_t.modify (a_itr, get_self(), [&](auto &a) {
      a.owner_attributes[attribute_name] = attribute_value; 
   });
}

// TODO: need to add security
void iopw::setattrs (const uint64_t& asset_id,
                     const map<string, string> attributes) 
{
   asset_table a_t (get_self(), get_self().value);
   auto a_itr = a_t.find (asset_id);
   check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

   a_t.modify (a_itr, get_self(), [&](auto &a) {
      a.owner_attributes = attributes; 
   });
}

// void iopw::setprice (const uint64_t& asset_id,
//                      const asset&   price) 
// {
//    asset_table a_t (get_self(), get_self().value);
//    auto a_itr = a_t.find (asset_id);
//    check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

//    require_auth (a_itr->owner);

//    a_t.modify (a_itr, get_self(), [&](auto &a) {
//       a.offer_price = price; 
//    });
// }

// void iopw::setbid (const uint64_t& asset_id,
//                      const name& bidder,
//                      const asset&   bid_amount)
// {
//    asset_table a_t (get_self(), get_self().value);
//    auto a_itr = a_t.find (asset_id);
//    check (a_itr != a_t.end(), "Asset not found. asset_id: " + std::to_string(asset_id));

//    require_auth (bidder);

//    confirm_balance (bidder, bid_amount);      

//    check (bid_amount > a_itr->highest_bid, "Bid amount must be greater than the current highest bid. Highest bid: " + 
//       a_itr->highest_bid.to_string() + "; Your bid: " + bid_amount.to_string());

//    a_t.modify (a_itr, get_self(), [&](auto &a) {
//       if (bid_amount >= a_itr->offer_price) {
//          string memo { "Purchase of asset: " + a_itr->asset_name };
//          sendfrombal (bidder, a_itr->owner, a_itr->offer_price, memo);

//          accounts acct_t (get_self(), a_itr->owner.value);
//          auto acct_itr = acct_t.find (a_itr->asset_symbol.code().raw());
//          check (acct_itr != acct_t.end(), "Asset symbol not found in accounts table. Asset symbol: " + a_itr->asset_symbol.code().to_string());

//          string memo1 { "Transfer of asset: " + a_itr->asset_name };
//          action(
//             permission_level{get_self(), "active"_n},
//             get_self(), "transfer"_n,
//             std::make_tuple(a_itr->owner, bidder, acct_itr->balance, memo1))
//          .send();   

//          a.owner              = bidder;
//          a.highest_bid        *= 0;
//          a.highest_bidder     = name(0);
//       } else {
//          a.highest_bid        = bid_amount;
//          a.highest_bidder     = bidder; 
//       } 
//    });
// }

void iopw::sell (const name& seller,
                  const asset& amount_to_sell) 
{
   require_auth (seller);

   // calculate price
   asset price = get_sales_price (amount_to_sell);

   print (" Price (releasing to seller)               : ", price.to_string(), "\n");
   print (" Burning CTB tokens                        : ", amount_to_sell.to_string(), "\n");

   float unit_price = (float) price.amount / (float) amount_to_sell.amount;
   print (" (Unit Price of CTB Token)                 : ", std::to_string(unit_price), "\n");

   string memo { "Sale from Curved Token Bonding -- IOPW"};

   // Transfer IOPW asset being sold to self
   transfer (seller, get_self(), amount_to_sell, memo);
   
   // Retire/burn the IOPW asset being sold
   string retire_memo { "Retire/burn tokens from CTB" };
   action(
      permission_level{get_self(), "active"_n},
      get_self(), "retire"_n,
      std::make_tuple(amount_to_sell, retire_memo))
   .send();  

   // Transfer TLOSGFT to seller
   action(
      permission_level{get_self(), "active"_n},
      get_config().bidding_token_contract, "transfer"_n,
      std::make_tuple(get_self(), seller, price, memo))
   .send(); 
}

void iopw::buy (const name& buyer, 
                  const asset& tlosgft_to_spend,
                  const symbol& symbol_to_buy) 
{  
   require_auth (buyer);

   asset purchase_qty = get_purchase_qty (tlosgft_to_spend, symbol_to_buy);
   
   print (" Purchase Price (saving in CTB Escrow)  : ", tlosgft_to_spend.to_string(), "\n");
   print (" Issuing CTB tokens to buyer            : ", purchase_qty.to_string(), "\n");

   float unit_price = (float) tlosgft_to_spend.amount / (float) purchase_qty.amount;
   print (" (Unit Price of CTB Token)              : ", std::to_string(unit_price), "\n");

   string memo { "Purchase from Curved Token Bonding -- IOPW"};
   sendfrombal (buyer, get_self(), tlosgft_to_spend, memo);

   string issue_memo { "Issue in order to transfer" };
   action(
      permission_level{get_self(), "active"_n},
      get_self(), "issue"_n,
      std::make_tuple(get_self(), purchase_qty, issue_memo))
   .send();   

   action(
      permission_level{get_self(), "active"_n},
      get_self(), "transfer"_n,
      std::make_tuple(get_self(), buyer, purchase_qty, memo))
   .send();   
}

void iopw::create(const name &creator,
                  const string& asset_name,
                  const string& asset_description,
                  const string& asset_category,
                  const string& asset_subcategory,
                  const symbol& asset_symbol,
                  const map<string, string> attribute_pairs)
{
   require_auth( creator );

   check( asset_symbol.is_valid(), "invalid symbol name" );

   stats statstable( get_self(), asset_symbol.code().raw() );
   auto existing = statstable.find( asset_symbol.code().raw() );
   check( existing == statstable.end(), "iopw with symbol already exists" );

   statstable.emplace( get_self(), [&]( auto& s ) {
      s.supply.symbol = asset_symbol;
      s.max_supply    = asset { -1, asset_symbol };
      s.issuer        = get_self();
   });

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());

   asset_table a_t (get_self(), get_self().value);
   a_t.emplace (get_self(), [&](auto &a) {
      a.asset_id              = hash (asset_name);
      a.creator               = creator;
      a.asset_name            = asset_name + ".iopw";
      a.asset_description     = asset_description;
      a.asset_category        = asset_category;
      a.asset_subcategory     = asset_subcategory;
      a.asset_symbol          = asset_symbol;
      a.immutable_attributes  = attribute_pairs;

      a.cost_multiplier       = c.cost_multiplier;
      a.base_cost             = c.base_cost;
      a.recovery_share        = c.recovery_share;
   });

   // string memo { "Initial Issuance" };
   // issue (creator, asset_supply, memo);
   // transfer (creator, c.initial_owner, asset_supply, memo);
}

void iopw::issue( const name& to, const asset& quantity, const string& memo )
{
   auto sym = quantity.symbol;
   check( sym.is_valid(), "invalid symbol name" );
   check( memo.size() <= 256, "memo has more than 256 bytes" );

   stats statstable( get_self(), sym.code().raw() );
   auto existing = statstable.find( sym.code().raw() );
   check( existing != statstable.end(), "iopw with symbol does not exist, create iopw before issue" );
   const auto& st = *existing;
   check( to == st.issuer, "iopws can only be issued to issuer account" );

   require_auth( st.issuer );
   check( quantity.is_valid(), "invalid quantity" );
   check( quantity.amount > 0, "must issue positive quantity" );

   check( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
   
   if (st.max_supply.amount >= 0) {
      check( quantity.amount <= st.max_supply.amount - st.supply.amount, "quantity exceeds available supply (max_supply is >= 0)");
   }
   
   statstable.modify( st, same_payer, [&]( auto& s ) {
      s.supply += quantity;
   });

   add_balance( st.issuer, quantity, st.issuer );
}

void iopw::retire( const asset& quantity, const string& memo )
{
   auto sym = quantity.symbol;
   check( sym.is_valid(), "invalid symbol name" );
   check( memo.size() <= 256, "memo has more than 256 bytes" );

   stats statstable( get_self(), sym.code().raw() );
   auto existing = statstable.find( sym.code().raw() );
   check( existing != statstable.end(), "iopw with symbol does not exist" );
   const auto& st = *existing;

   require_auth( st.issuer );
   check( quantity.is_valid(), "invalid quantity" );
   check( quantity.amount > 0, "must retire positive quantity" );

   check( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );

   statstable.modify( st, same_payer, [&]( auto& s ) {
      s.supply -= quantity;
   });

   sub_balance( st.issuer, quantity );
}

void iopw::transfer( const name&    from,
                      const name&    to,
                      const asset&   quantity,
                      const string&  memo )
{
   // // no support for shares yet - transfer the whole thing
   // asset_table a_t (get_self(), get_self().value);
   // auto symbol_index = a_t.get_index<"bysymbol"_n>();
   // auto a_itr = symbol_index.find (quantity.symbol.code().raw());
   // check (a_itr != symbol_index.end(), "Asset not found in assets table: " + quantity.to_string());
   // symbol_index.modify (a_itr, get_self(), [&](auto a) {
   //    a.owner     = to;
   // });

   check( from != to, "cannot transfer to self" );

   check (has_auth (from) || has_auth (get_self()), "Permission denied. Must have permission from owner or assets contract.");
   // require_auth( from );
   check( is_account( to ), "to account does not exist");
   auto sym = quantity.symbol.code();
   stats statstable( get_self(), sym.raw() );
   const auto& st = statstable.get( sym.raw() );

   require_recipient( from );
   require_recipient( to );

   check( quantity.is_valid(), "invalid quantity" );
   check( quantity.amount > 0, "must transfer positive quantity" );
   check( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
   check( memo.size() <= 256, "memo has more than 256 bytes" );

   auto payer = has_auth (to) ? to : has_auth( from ) ? from : get_self();

   sub_balance( from, quantity );
   add_balance( to, quantity, payer );
}

void iopw::sub_balance( const name& owner, const asset& value ) {
   accounts from_acnts( get_self(), owner.value );

   const auto& from = from_acnts.get( value.symbol.code().raw(), "no balance object found" );
   check( from.balance.amount >= value.amount, "overdrawn balance" );

   from_acnts.modify( from, get_self(), [&]( auto& a ) {
         a.balance -= value;
      });
}

void iopw::add_balance( const name& owner, const asset& value, const name& ram_payer )
{
   accounts to_acnts( get_self(), owner.value );
   auto to = to_acnts.find( value.symbol.code().raw() );
   if( to == to_acnts.end() ) {
      to_acnts.emplace( get_self(), [&]( auto& a ){
        a.balance = value;
      });
   } else {
      to_acnts.modify( to, same_payer, [&]( auto& a ) {
        a.balance += value;
      });
   }
}

void iopw::open( const name& owner, const symbol& symbol, const name& ram_payer )
{
   require_auth( ram_payer );

   check( is_account( owner ), "owner account does not exist" );

   auto sym_code_raw = symbol.code().raw();
   stats statstable( get_self(), sym_code_raw );
   const auto& st = statstable.get( sym_code_raw, "symbol does not exist" );
   check( st.supply.symbol == symbol, "symbol precision mismatch" );

   accounts acnts( get_self(), owner.value );
   auto it = acnts.find( sym_code_raw );
   if( it == acnts.end() ) {
      acnts.emplace( get_self(), [&]( auto& a ){
        a.balance = asset{0, symbol};
      });
   }
}

void iopw::close( const name& owner, const symbol& symbol )
{
   require_auth( owner );
   accounts acnts( get_self(), owner.value );
   auto it = acnts.find( symbol.code().raw() );
   check( it != acnts.end(), "Balance row already deleted or never existed. Action won't have any effect." );
   check( it->balance.amount == 0, "Cannot close because the balance is not zero." );
   acnts.erase( it );
}

void iopw::deposit ( const name& from, const name& to, const asset& quantity, const string& memo ) {

   //eosio::check (!is_paused(), "Contract is paused - no actions allowed.");
   if (to != get_self()) { return; }  // not sending to gyftieassets
   if (memo == "BYPASS") { return; }   // use memo of BYPASS to transfer without recording

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   check (quantity.symbol == c.bidding_token_symbol, "Only deposits of then bidding token symbol are allowed. Bidding token: " +
      c.bidding_token_symbol.code().to_string() + "; You sent: " + quantity.symbol.code().to_string());
  
   check (get_first_receiver() == c.bidding_token_contract,  
      "Only deposits of the bidding token contract are accepted. Bidding contract: " + c.bidding_token_contract.to_string() + 
      "; Your token contract: " + get_first_receiver().to_string());
  
   balance_table balances(get_self(), from.value);
   asset new_balance;
   auto it = balances.find(quantity.symbol.code().raw());
   if(it != balances.end()) {
      check (it->token_contract == get_first_receiver(), "Transfer does not match existing token contract.");
      balances.modify(it, get_self(), [&](auto& bal){
         // Assumption: total currency issued by eosio.token will not overflow asset
         bal.funds += quantity;
         new_balance = bal.funds;
      });
   }
   else {
      balances.emplace(get_self(), [&](auto& bal){
         bal.funds = quantity;
         bal.token_contract  = get_first_receiver();
         new_balance = quantity;
      });
   }
}
