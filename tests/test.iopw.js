/* eslint-disable prettier/prettier */
const assert = require('assert');
const eoslime = require('eoslime').init('local');

const IOPW_WASM_PATH = '../iopw/iopw.wasm';
const IOPW_ABI_PATH = '../iopw/iopw.abi';
const EOSIOTOKEN_WASM_PATH = '../../../token/token/token.wasm';
const EOSIOTOKEN_ABI_PATH = '../../../token/token/token.abi';
 
describe('IOPW Testing', function () {

    // Increase mocha(testing framework) time, otherwise tests fails
    this.timeout(150000);

    let iopwContract, iopwAccount; 
    let eosioTokenContract, eosioTokenAccount, accounts;

    async function getAssets () {
        let assetTable = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: iopwAccount.name,
            table: 'assets',
            json: true
        });
        // console.log (assetTable);
        return assetTable;
    }

    async function getBalances (user) {
    
        const accounts = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: user.name,
            table: "accounts",
            json: true
        });
        console.log (user.name, ": IOPW Balances                             : ", accounts);
        return accounts;
    }

    async function getGft (user) {
        const accounts = await iopwContract.provider.eos.getTableRows({
            code: eosioTokenAccount.name,
            scope: user.name,
            table: "accounts",
            json: true
        });
        if (accounts.rows.length == 0) {
            console.log(user.name, ": TLOSGFT Balance from Token Contract       : 0.00000000 TLOSGFT");
            return "0.00000000 TLOSGFT";
        }
        console.log(user.name, ": TLOSGFT Balance from Token Contract       : ", accounts.rows[0].balance);
        return accounts.rows[0].balance;
    }

    async function getIOPWSupply (symbol) {
        const stat = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: symbol,
            table: "stat",
            json: true
        });
        if (stat.rows.length == 0) {
            console.log(symbol, ": Supply        : 0.00000000 ", symbol);
            return "0.00000000 " + symbol;
        }
        console.log(symbol, ": Supply   : ", stat.rows[0].supply);
        return stat.rows[0].supply;
    }

    async function getIOPWGftBalance (user) {
        const accounts = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: user.name,
            table: "balances",
            json: true
        });
        if (accounts.rows.length == 0) {
            console.log(user.name, ": TLOSGFT Balance from IOPW Contract        : 0.00000000 TLOSGFT");
            return "0.00000000 TLOSGFT";
        }
        console.log(user.name, ": TLOSGFT Balance from IOPW Contract        : ", accounts.rows[0].funds);
        return accounts.rows[0].funds;
    }

    before(async () => {

        accounts = await eoslime.Account.createRandoms(20);
        iopwAccount         = accounts[1];
        
        user1               = accounts[4];
        user2               = accounts[5];
        user3               = accounts[6];
        user4               = accounts[7];
        user5               = accounts[8];
        user6               = accounts[9];
        user7               = accounts[10];
        // biddingTokenAccount = accounts[8];
        eosioTokenAccount   = accounts[19];
       
        console.log (" IOPW             : ", iopwAccount.name)
        console.log (" user1            : ", user1.name);
        console.log (" user2            : ", user2.name);
        console.log (" user3            : ", user3.name);
        console.log (" TLOSGFT Token    : ", eosioTokenAccount.name)

        await iopwAccount.addPermission ('eosio.code');
        const initialBalance = '1000.00000000 TLOSGFT';

        eosioTokenContract = await eoslime.AccountDeployer.deploy (EOSIOTOKEN_WASM_PATH, EOSIOTOKEN_ABI_PATH, eosioTokenAccount);
        await eosioTokenContract.create(eosioTokenContract.name, '100000.00000000 TLOSGFT');

        console.log ("\n\n");
        console.log ("Test user accounts each receive 1000.00000000 TLOSGFT.")
        await eosioTokenContract.issue(eosioTokenAccount.name, "100000.00000000 TLOSGFT", 'memo', { from: eosioTokenAccount});
        for (const user of [user1, user2, user3, user7]) {
            await eosioTokenContract.transfer(eosioTokenAccount.name, user.name, initialBalance, 'memo', { from: eosioTokenAccount});
        }
        //     await eosioTokenContract.transfer(eosioTokenAccount.name, user2.name, initialBalance, 'memo', { from: eosioTokenAccount});
        // await eosioTokenContract.transfer(eosioTokenAccount.name, user3.name, initialBalance, 'memo', { from: eosioTokenAccount});

        iopwContract = await eoslime.AccountDeployer.deploy (IOPW_WASM_PATH, IOPW_ABI_PATH, iopwAccount);
        await iopwContract.setconfig (eosioTokenAccount.name, "8,TLOSGFT", 1.0100000000, "0.01000000 TLOSGFT", "0.99000000000000", { from: iopwAccount });
    });

    beforeEach(async () => {
   
    });

    it('Test create an iopw asset', async () => {

        const imAttr = [{
            "key":"accountName",
            "value":"@edsheeran"
        }];

        await iopwContract.create(user1.name, "Ed Sheeran", "Instagram.com account for Ed Sheeran", "Online Accounts", "Instagram",
            "8,ESN", imAttr, { from: user1 });

        let assetTable = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: iopwAccount.name,
            table: 'assets',
            json: true
        });
        console.log( JSON.stringify(assetTable, null, 2));
        assert.equal (assetTable.rows[0].asset_name, "Ed Sheeran.iopw", "Object did not retain asset_name"); 
        assert.equal (assetTable.rows[0].immutable_attributes[0].key, "accountName", "Object does not have immutable attribute key: accountName");
        assert.equal (assetTable.rows[0].immutable_attributes[0].value, "@edsheeran", "Object does not have immutable attribute value: accountName/@edsheeran");
    });

    it('Test add an attribute', async () => {

        await iopwContract.addattribute (4135626856, "Followers", "4000", { from: user1 });

        let assetTable = await getAssets ();
        // console.log( JSON.stringify(assetTable, null, 2));
        assert.equal (assetTable.rows[0].asset_name, "Ed Sheeran.iopw", "Object did not retain asset_name"); 
        assert.equal (assetTable.rows[0].owner_attributes[0].key, "Followers", "Object did not retain owner attribute");
        assert.equal (assetTable.rows[0].owner_attributes[0].value, "4000", "Object does not have owner attribute value");
    });

    it('Test edit an attribute', async () => {

        await iopwContract.editattr (4135626856, "Followers", "4001", { from: user1 });

        let assetTable = await getAssets ();
        // console.log( JSON.stringify(assetTable, null, 2));
        assert.equal (assetTable.rows[0].asset_name, "Ed Sheeran.iopw", "Object did not retain asset_name"); 
        assert.equal (assetTable.rows[0].owner_attributes[0].key, "Followers", "Object did not retain owner attribute");
        assert.equal (assetTable.rows[0].owner_attributes[0].value, "4001", "Object does not have owner attribute value");
    });

    it('Test remove an attribute', async () => {

        await iopwContract.remattribute (4135626856, "Followers", { from: user1 });
        let assetTable = await getAssets ();
        // console.log( JSON.stringify(assetTable, null, 2));
        assert.equal (assetTable.rows[0].asset_name, "Ed Sheeran.iopw", "Object did not retain asset_name"); 
        assert.strictEqual (assetTable.rows[0].owner_attributes.length, 0, "Followers attribute was not removed.");
    });

    it('The owner can set attributes', async () => {

        const ownerAttr = [{
            "key":"Followers",
            "value":"1,000,000"
        },
        {
            "key":"Joined Date",
            "value":"12 January 2016"
        }];

        await iopwContract.setattrs (4135626856, ownerAttr, { from: iopwAccount });

        let assetTable = await getAssets ();
        console.log( JSON.stringify(assetTable, null, 2));
        assert.strictEqual (assetTable.rows[0].asset_name, "Ed Sheeran.iopw", "Object did not retain asset_name"); 
        assert.strictEqual (assetTable.rows[0].owner_attributes.length, 2, "Attributes were not added");
    });

    it('Test deposits', async () => {

        // users must deposit their tokens into the IOPW contract before they can purchase IOPW assets
        // this transfers 100 TLOSGFT from users 1-3 to the IOPW Contract, then tests that the balances were properly updated
       
        for (const user of [user1, user2, user3, user7]) {
            await eosioTokenContract.transfer (user.name, iopwContract.name, "100.00000000 TLOSGFT", "memo", { from: user });
            assert.strictEqual (await getIOPWGftBalance(user), "100.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
            assert.strictEqual (await getGft(user), "900.00000000 TLOSGFT", "TLOSGFT token quantity does not match");    
        }
    });

    it('Test a purchase', async () => {

        await iopwContract.buy (user3.name, "25.00000000 TLOSGFT", "8,ESN", { from: user3 });

        // after spending 25 TLOSGFT, the user is given 69.41083526 ESN
        let accountsTable = await getBalances(user3);
        assert.strictEqual (accountsTable.rows[0].balance, "326.95510864 ESN", "Asset of CBT did not match");

        assert.strictEqual (await getIOPWGftBalance(user3), "75.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
        assert.strictEqual (await getGft(user3), "900.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
    });

    it('Test a purchase from two users', async () => {

        // these two purchasers purchase the same amount, but the first one gets a better price

        // user2 purchases 10 TLOSGFT of ESN, verifies receives ESN and deducts TLOSGFT
        assert.strictEqual (await getIOPWGftBalance(user2), "100.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
        await iopwContract.buy (user2.name, "10.00000000 TLOSGFT", "8,ESN", { from: user2 });
        assert.strictEqual (await getIOPWGftBalance(user2), "90.00000000 TLOSGFT", "TLOSGFT token quantity does not match");

        let accountsTable = await getBalances(user2);
        assert.strictEqual (accountsTable.rows[0].balance, "32.69934463 ESN", "Asset of CBT did not match");

        // user3 purchases same -- this verifies they pay a different price
        assert.strictEqual (await getIOPWGftBalance(user1), "100.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
        await iopwContract.buy (user1.name, "10.00000000 TLOSGFT", "8,ESN", { from: user1 });
        assert.strictEqual (await getIOPWGftBalance(user1), "90.00000000 TLOSGFT", "TLOSGFT token quantity does not match");

        accountsTable = await getBalances(user1);
        assert.strictEqual (accountsTable.rows[0].balance, "24.63159942 ESN", "Asset of CBT did not match");
    });

    it('Test a transfer of the CBT token', async () => {

        // user1 transfers 1 ESN to user5
        await iopwContract.transfer (user1.name, user5.name, "1.00000000 ESN", "Memo", { from: user1 });

        // confirm that user5 receives 1 ESN and it is deducted from user1's balance
        let accountsTable = await getBalances(user5);
        assert.strictEqual (accountsTable.rows[0].balance, "1.00000000 ESN", "Asset of CBT did not match");

        accountsTable = await getBalances(user1);
        assert.strictEqual (accountsTable.rows[0].balance, "23.63159942 ESN", "Asset of CBT did not match");
    });

    it('Test selling the CBT token', async () => {

        // before the sale, user5 has 0 TLOSGFT and the supply of ESN is 138....
        assert.strictEqual (await getGft(user5), "0.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
        assert.strictEqual (await getIOPWSupply("ESN"), "384.28605269 ESN", "Asset of CBT did not match.");

        // sells 1 ESN
        await iopwContract.sell (user5.name, "1.00000000 ESN", { from: user5 });

        // after the sale, user5 has some TLOSGFT and the supply of ESN has decreased by 1 to show it was burned
        assert.strictEqual (await getGft(user5), "0.45093968 TLOSGFT", "TLOSGFT token quantity does not match");
        assert.strictEqual (await getIOPWSupply("ESN"), "383.28605269 ESN", "Asset of CBT did not match.");

        // and user5 no longer has the ESN
        let accountsTable = await getBalances(user5);
        assert.strictEqual (accountsTable.rows[0].balance, "0.00000000 ESN", "Asset of CBT did not match");
    });

    it('Create a second iopw asset', async () => {

        const imAttr = [{
            "key":"accountName",
            "value":"@gomez"
        }];

        await iopwContract.create(user6.name, "Selena Gomez", "Instagram.com account for Selena Gomez", "Online Accounts", "Instagram",
            "8,GOMEZ", imAttr, { from: user6 });

        let assetTable = await iopwContract.provider.eos.getTableRows({
            code: iopwAccount.name,
            scope: iopwAccount.name,
            table: 'assets',
            json: true
        });
        // console.log( JSON.stringify(assetTable, null, 2));
        assert.equal (assetTable.rows[0].asset_name, "Selena Gomez.iopw", "Object did not retain asset_name"); 
        assert.equal (assetTable.rows[0].immutable_attributes[0].key, "accountName", "Object does not have immutable attribute key: accountName");
        assert.equal (assetTable.rows[0].immutable_attributes[0].value, "@gomez", "Object does not have immutable attribute value: accountName/@gomez");
    });

    it('Test a purchase on a second item', async () => {

        await iopwContract.buy (user7.name, "0.01000000 TLOSGFT", "8,GOMEZ", { from: user7 });

        // after spending 0.01 TLOSGFT, the user is given  
        let accountsTable = await getBalances(user7);
        assert.strictEqual (accountsTable.rows[0].balance, "0.99505347 GOMEZ", "Asset of CBT did not match");

        assert.strictEqual (await getIOPWGftBalance(user7), "99.99000000 TLOSGFT", "TLOSGFT token quantity does not match");
        assert.strictEqual (await getGft(user7), "900.00000000 TLOSGFT", "TLOSGFT token quantity does not match");
    });

});