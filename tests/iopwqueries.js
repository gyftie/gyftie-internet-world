const { Api, JsonRpc } = require("eosjs");
const fetch = require("node-fetch"); // node only; not needed in browsers
require ("dotenv/config");

async function printAll () {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "gyftieassets";
  options.scope = "gyftieassets";

  options.json = true;
  options.table = "assets";

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no assets with symbol: ", comm);
    }
  })
}

async function printByAssetId (asset_id) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "gyftieassets";
  options.scope = "gyftieassets";

  options.json = true;
  options.table = "assets";
  options.lower_bound = asset_id; 
  options.upper_bound = asset_id; 

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no assets with asset_id: ", asset_id);
    }
  })
}

async function printBySymbol (symbol) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "gyftieassets";
  options.scope = "gyftieassets";

  options.json = true;
  options.table = "assets";
  options.index_position = 5; // index #5 is "bysymbol"
  options.key_type = 'i64';
  options.lower_bound = symbol; 
  options.upper_bound = symbol;

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no assets with symbol: ", symbol);
    }
  })
}

async function printBalancesByAccount (account) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "gyftieassets";
  options.scope = account;

  options.json = true;
  options.table = "accounts";

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no assets with symbol: ", symbol);
    }
  })
}

async function printBalancesAndAssetDetailsByAccount (account) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "gyftieassets";
  options.scope = account;

  options.json = true;
  options.table = "accounts";

  const result = await rpc.get_table_rows(options); 
  
  if (result.rows.length > 0) {
    result.rows.forEach (function(symbol) {
      printBySymbol (symbol.balance.substr(symbol.balance.indexOf(" ") + 1, symbol.balance.length));
    });
    console.log (JSON.stringify(result.rows, null, 2));
  } else {
    console.log ("There are no assets with symbol: ", symbol);
  }
}


async function main () {
  try {

    // Print All Assets and their details
    // await printAll ();

    // Print a specific asset's details based on the asset_id
    // await printByAssetId (3786925672);

    // Print a specific asset's details based on the symbol
    // await printBySymbol ("KYLIEJ");

    // Print asset balances for a specific account
    // await printBalancesByAccount ("agyftieuser2");

    // Print asset balances for a specific account and all of the corresponding asset details
    // await printBalancesAndAssetDetailsByAccount ("agyftieuser2");

  } catch (e) {
    console.log (e);
  }
}

main ();

